lazy val root = (project in file(".")).
	settings(
		name := "Analyses Collection",
		version := "0.0.1",
		scalaVersion := "2.11.8"
	)

libraryDependencies += "de.opal-project" % "abstract-interpretation-framework_2.11" % "0.0.2"